'use strict' ;
 
module.exports = function(grunt) {
    //Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    //Automatically load required Grunt tasks
    require('jit-grunt')(grunt);

    //Define the configuration for all the tasks
    grunt.initConfig({
        sass: {
            dist: {
                files: {
                    'css/styles.css': 'css/styles.scss'   
                     }
            }
        },

        watch: {
            files: 'css./*.scsss',
            tasks:['sass']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src:[
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options:{
                    watchTask: true,
                    server: {
                        baseDir: './'
                    }
                }
            }
        }
    });

    grunt.registerTask('css', ['sass']); //to execute the task. The task's name is 'css' and this task involves executing the 'sass' task
    grunt.registerTask('default',['browserSync','watch']);
};